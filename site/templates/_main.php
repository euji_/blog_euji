<?php namespace ProcessWire;

// Optional main output file, called after rendering page’s template file. 
// This is defined by $config->appendTemplateFile in /site/config.php, and
// is typically used to define and output markup common among most pages.
// 	
// When the Markup Regions feature is used, template files can prepend, append,
// replace or delete any element defined here that has an "id" attribute. 
// https://processwire.com/docs/front-end/output/markup-regions/
	
/** @var Page $page */
/** @var Pages $pages */
/** @var Config $config */

$home = $pages->get('/');
$linkslist = $pages->get('/linkslist/'); // homepage
$feed = $pages->get('/feed/');
$postsblog = $feed->children;
$categoriesPage = $pages->get('/categories/');
$categories = $categoriesPage->children;





?><!DOCTYPE html>

<html lang="en">
	<head id="html-head">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title><?php echo $page->title; ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>styles/main.css" />
		<script src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>
	</head>
	<body id="html-body">

	<header>

	<!-- todo : make the banner work -->
		<?php 
		
		echo "<a href='$home->url'><img src='site/assets/files/img/banner.jpg' alt='blog-banner-image'></a>";
		
		
		?>
		

		<nav id="uppernav">

		<?php 



			
			

foreach($categories as $category) {

				echo "<li><a href='$category->url'>" . $category->title . '</a></li>';
			}
			

			// Récupérer tous les enfants de la page "feed"
			
			echo "</ul>";



			 ?>

		</nav>
	</header>

	<nav id="sidenav">
		



<div class="post-by-title">
<h2>Posts par titre</h2>
				
				<?php



			foreach($postsblog as $post) {

				$posturl = $post->url;
				$title = $post->title;
				$date = $post->date;
				echo "<a href='$posturl'>$date - $title</a>";
			}


				?>

				
		</div>

			<?php 

$posts = $linkslist->children;
			  

echo "</div></nav>";

?>
		
<!-- 		
		<h1 id="headline">
			<?php if($page->parents->count()): // breadcrumbs ?>
				<?php echo $page->parents->implode(" &gt; ", "<a href='{url}'>{title}</a>"); ?> &gt;
			<?php endif; ?>
			
			<?php echo $page->title; // headline ?> -->
		</h1>
		
		<div id="content">
		
		</div>
	
		<ul id="subnav">
			<?php 
			echo "<li><a href='#top'>↑ Retour en haut</a></li>";
			
			$posts = $linkslist->children;
			  
			  

			  foreach($posts as $post) {
			  
				$title = $post->title;
				$linkurl = $post->linkurl;
			

					echo "<li><a class='linkurl' href='$linkurl'>$title</a></li>"  ;


			  }
			  
			
			  
			  // subnav ?>
		</ul>	
		
		<?php if($page->editable()): ?>
		<p><a href='<?php echo $page->editUrl(); ?>'>Edit this page</a></p>
		<?php endif; ?>
	
	</body>
</html>
